from .keys import OPEN_WEATHER_KEY, PEXELS_API_KEY
import requests
import json


def get_city_image_url(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}-{state}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    image = json.loads(response.content)
    try:
        return image["photos"][0]["url"]
    except (KeyError, IndexError):
        return None


def get_weather(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "Limit": 1,
        "appid": OPEN_WEATHER_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None
    
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_KEY,
        "units": "imperial"
    }

    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "weather_description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except(KeyError, IndexError):
        return None
